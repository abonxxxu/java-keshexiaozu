package FiveChess;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JOptionPane;



public class MainPanel extends Panel implements MouseListener{
	private static final int line = 16;//列数
	private static final int row = 16;//行数
	private static final int gap = 40;//间距
	private static boolean isBlack = true;
	private static int click_X;
	private static int click_Y;
	private char[][] Chess= new char[row][line];
	boolean gameOver=false;
	Image img;

	public MainPanel(){
		addMouseListener(this);  
		addMouseMotionListener(new MouseMotionListener(){  
	           public void mouseDragged(MouseEvent e){  
	                 
	           }  
	             
	           public void mouseMoved(MouseEvent e){  
	             int x1=(e.getX()-20+gap/2)/gap;  
	             //将鼠标点击的坐标位置转成网格索引  
	             int y1=(e.getY()-20+gap/2)/gap;  
	             //游戏已经结束不能下  
	             //落在棋盘外不能下  
	             //x，y位置已经有棋子存在，不能下  
	             if(x1<0||x1>row||y1<0||y1>line||gameOver)  
	                 setCursor(new Cursor(Cursor.DEFAULT_CURSOR));  
	             //设置成默认状态  
	             else setCursor(new Cursor(Cursor.HAND_CURSOR));  
	               
	           }  
	       });  
		for(int i=0;i<Chess.length;i++){
			for(int j=0;j<Chess[i].length;j++){
				Chess[i][j]='0';//棋子的坐标  令棋子为空
			}
		}
	}
	
	
    public void paint(Graphics g){//制图
    	 img=Toolkit.getDefaultToolkit().getImage("board.jpg"); 
    	 setBackground(new Color(209, 167, 78));
    	for(int i=0;i<row;i++){//划横线
    		g.setColor(Color.BLACK);
    		g.drawLine(20, 20+i*gap, 640-20, 20+i*gap);//y1=y2 ,边距为20 
    	}
    	for(int i=0;i<line;i++){//划纵线
    		g.setColor(Color.BLACK);
    		g.drawLine(20+i*gap, 20, 20+i*gap, 640-20);
    	}
    	for(int i=0;i<Chess.length;i++){
    		for(int j=0;j<Chess[i].length;j++){
    			if(Chess[i][j]=='1'){//“1”代表白棋
    				g.setColor(Color.WHITE);
    				g.fillOval(5+i*gap, 5+j*gap, 30, 30);//填充棋子
    				g.drawOval(5+i*gap, 5+j*gap, 30, 30);//画圆（）
    			}
    			if(Chess[i][j]=='2'){
    				g.setColor(Color.BLACK);//“0”代表黑棋
    				g.fillOval(5+i*40, 5+j*40, 30, 30);
    				g.drawOval(5+i*40, 5+j*40, 30, 30);
    			}
    		}
    	}
    }
    public boolean isWin(int x,int y,boolean isColor){
    	char ch=Chess[x][y];
    	/*  横向判断  ，先左后右 */
    	int LX = x;
    	while(LX>=0 && Chess[LX][y]==ch){
    		LX --;
    	}
        int LNum = 0;//统计横向相同的棋子数
        LX ++; 
    	while(LX<Chess.length && Chess[LX][y]==ch){
    		LNum ++;
    		LX ++;
    	}
    	/* 纵向判断 ，先上后下 */
    	int RY = y;
    	while(RY>=0 && Chess[x][RY]==ch){
    		RY --;//坐标位置下降
    	}
    	int RNum = 0;//统计纵向相同的棋子数
    	RY ++;
    	while(RY<Chess[x].length && Chess[x][RY]==ch){
    		RY ++;
    		RNum ++;
    	}
    	/* 左下右上判断  ，*/
    	int LDX = x;
    	int RUY = y;
    	while(LDX>=0 && RUY<Chess.length && Chess[LDX][RUY]==ch){
    		LDX --;
    		RUY ++;
    	}
    	int LDNum = 0;
    	LDX ++;
    	RUY --;
    	while(LDX<Chess.length && RUY>=0 && Chess[LDX][RUY]==ch){
    		LDNum ++;
    		LDX ++;
    		RUY --;
    	}
    	/* 右上左下判断  */
    	int RUX = x;
    	int LDY = y;
    	while(RUX>=0 && LDY>=0 && Chess[RUX][LDY]==ch){
    		RUX --;
    		LDY --;
    	}
    	int RUNum = 0;
    	RUX ++;
    	LDY ++;
    	while(RUX>=0 && LDY<Chess.length && Chess[RUX][LDY]==ch){
    		RUX ++;
    		LDY ++;
    		RUNum ++;
    	}
    	if(RNum>=5||LNum>=5||RUNum>=5||LDNum>=5){                                            
    		return true;
    	}
    	return false;
    } 

	public void mouseClicked(MouseEvent e) {
		
	}	
	public void mousePressed(MouseEvent e) {//鼠标点击事件处理过程, 在源组件上按下鼠标按钮
		if(gameOver) return;
		int click_x = e.getX();
		int click_y = e.getY();
		int chess_x = Math.round((float)(click_x-20)/gap);//画一个区域这个区域都为对应的棋子坐标点
		int chess_y = Math.round((float)(click_y-20)/gap);
		click_X = chess_x;
		click_Y = chess_y;
        if(isBlack==true&&Chess[chess_x][chess_y]=='0'){
		    Chess[chess_x][chess_y] = '2';
		    isBlack = false;//换为白子
        }
        if(isBlack==false&&Chess[chess_x][chess_y]=='0'){
        	Chess[chess_x][chess_y] = '1';
        	isBlack = true;//换为黑子
        }
        repaint();//重新扫描，图像修改
	    if(isWin(chess_x,chess_y,isBlack)){
	    	if(isBlack){
	    	    JOptionPane.showMessageDialog(null,"白子赢了");
	    	   
	    	}else{
	    		JOptionPane.showMessageDialog(null,"黑子赢了");
	    		
	    	}
	    	gameOver=true;  
	    }
	}
	public void mouseReleased(MouseEvent e) {
		
	}// 释放源组件上的鼠标按钮
	public void mouseEntered(MouseEvent e) {
		
	}//在鼠标进入源组件之后被调用
	public void mouseExited(MouseEvent e) {
		
	}//在鼠标退出源组件之后被调用 为什么要用这几个 
	//public void setChess(char[][] Chess) {
		//this.Chess = Chess;
	//}
	//public char[][] getChess() {
		//return Chess;
	//}
	/*get(),set()方法就是对一个类中私有的属性对外可见,可以调用的方法,get()是得到某个属性的值
     ,而set则是根据传入的值改变该属性的值,
	这样的好处就是想要传出的属性和想要通过外部参数修改的属性只能通过给定的get(),set()方法来执行,
	而且里面具体的赋值或取得属性的方法和过程,外部都无法知道.*/

	public void restartGame(){  
	       //清除棋子  
		for(int i=0;i<Chess.length;i++){
			for(int j=0;j<Chess[i].length;j++){
				Chess[i][j]='0';//棋子的坐标  令棋子为空
			}
		}
	       //恢复游戏相关的变量值  
	       isBlack=true;  
	       gameOver=false; //游戏是否结束  
	       //chessCount =0; //当前棋盘棋子个数  
	       repaint();  
	   }  
	     
	   //悔棋  
	 
	     
	 
	
	
	     
 
}
